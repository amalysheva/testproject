package ru.neoflex.neostudy;

public class Circle extends Shape{

    private final Point a;
    private final double radius;

    public Circle(Point a, double radius) {
        super("Окружность");
        this.a = a;
        this.radius = radius;
    }

    @Override public double getLength() {
        return 2 * Math.PI * radius;
    }

    @Override public double getArea() {
        return Math.PI * Math.sqrt(radius);
    }

    @Override public String toString() {
        return "Circle{" +
                "type=" + getType() +
                ", a=" + a +
                ", radius=" + radius +
                '}';
    }
}
