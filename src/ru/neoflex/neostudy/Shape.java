package ru.neoflex.neostudy;

public abstract class Shape {
    private String type;

    public Shape(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public abstract double getLength();
    public abstract double getArea();

}
