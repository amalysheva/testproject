package ru.neoflex.neostudy;

public class Line extends Shape {
    private final Point begin;
    private final Point end;

    public Line(Point begin, Point end) {
        super("Линия");
        this.begin = begin;
        this.end = end;
    }

    public double getLength() {
        double dx = end.getX() - begin.getX();
        double dy = end.getY() - begin.getY();

        return Math.sqrt(dx * dx + dy * dy);
    }
    public double getArea() {
        return 0;
    }

    @Override public String toString() {
        return "Line{" +
                "type=" + getType() +
                ", begin=" + begin +
                ", end=" + end +
                '}';
    }
}
