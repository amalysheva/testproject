package ru.neoflex.neostudy;

class Color {
    public static final Color RED = new Color("red");
    public static final Color BLUE = new Color("blue");
    public static final Color GREEN = new Color("green");

    private String name;

    private Color(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }
}
