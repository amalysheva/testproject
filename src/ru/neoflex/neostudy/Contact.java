package ru.neoflex.neostudy;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Contact {
    private String name;
    private List<String> phones = new ArrayList<>();

    public Contact(String name, List<String> phones) {
        this.name = name;
        this.phones.addAll(phones);
    }

    public Contact(String name, String phone) {
        this.name = name;
        this.phones.add(phone);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getPhones() {
        return phones;
    }

    public void addPhone(String phone) {
        this.phones.add(phone);
    }

    @Override public boolean equals(Object o) {
        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }
        Contact contact = (Contact) o;
        return Objects.equals(name, contact.name);
    }

    @Override public int hashCode() {
        return Objects.hash(name);
    }
}
