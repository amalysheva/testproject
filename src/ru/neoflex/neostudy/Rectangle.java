package ru.neoflex.neostudy;

public class Rectangle extends Shape{

    private final Line ab;
    private final Line bc;
    private final Line ad;
    private final Line dc;

    public Rectangle(Line ab, Line ad, Line bc, Line dc) {
        super("Треугольник");
        this.ab = ab;
        this.ad = ad;
        this.bc = bc;
        this.dc = dc;
    }

    @Override public double getLength() {
        double abDist = ab.getLength();
        double adDist = ad.getLength();
        double bcDist = bc.getLength();
        double dcDist = dc.getLength();

        return abDist + adDist + bcDist + dcDist;
    }

    @Override public double getArea() {
        double abDist = ab.getLength();
        double adDist = ad.getLength();
        double bcDist = bc.getLength();

        if (abDist == adDist && abDist == bcDist) {
            return abDist * adDist;
        } else if (abDist != adDist) {
            return abDist * adDist;
        } else {
            return abDist * bcDist;
        }
    }

    @Override public String toString() {
        return "Rectangle{" +
                "type=" + getType() +
                ", ab=" + ab +
                ", bc=" + bc +
                ", ad=" + ad +
                ", dc=" + dc +
                '}';
    }
}
