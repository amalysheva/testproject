package ru.neoflex.neostudy;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static int calc(int a, int b, int c, int d){
        return a * (b+(c/d));
    }

    public static boolean checkBetween(int a){
        return a >= 10 && a <= 20;
    }

    public static void print(int a){
        if (a >= 10){
            System.out.println("Введенное число " + a + " - положительное.");
        } else {
            System.out.println("Введенное число " + a + " - отрицательное.");
        }
    }

    public static boolean checkNegative(int a){
        return a < 10;
    }

    public static void printName(String name) {
        System.out.println("Привет, " + name);
    }

    public static void checkYear(int year) {
        if ((year % 4 == 0 && year % 100 !=0) || (year % 400 == 0)) {
            System.out.println("Да");
        } else {
            System.out.println("Нет");
        }
    }

    public static void main(String[] args) {
        byte val = -120;
        short valShort = 12442;
        int valInt = 1000;
        long valLong = 200000L;
        float valFloat = 12.23f;
        double valDouble = 123.123;
        char valChar = '\u2242';
        boolean valBool = false;

        System.out.println(calc(100, 200,300,400));
        System.out.println(checkBetween(11));
        print(-10);
        System.out.println(checkNegative(10));
        printName("Вася");
        checkYear(1600);
        checkYear(1700);

        Contact contact1 = new Contact("anna", "121212");
        Contact contact2 = new Contact("Anna123", "121213");

        ContactService.addContact(contact1);
        ContactService.addContact(contact2);

        ContactService.getContact("Anna");
        ContactService.getContactLikeName("Anna");
        ContactService.getPhonesByName("anna");

        Point point1 = new Point(1, 1);
        Point point2 = new Point(1, 2);
        Point point3 = new Point(2,2);
        Point point4 = new Point(2,1);

        Line line1 = new Line(point1, point2);
        Line line2 = new Line(point1, point4);
        Line line3 = new Line(point2, point3);
        Line line4 = new Line(point3, point4);
        Line line5 = new Line(point1, point3);

        Triangle triangle = new Triangle(line1,line3, line5);
        Rectangle rectangle = new Rectangle(line1, line2, line3, line4);
        Circle circle = new Circle(new Point(0,0), 1);
        List<Shape> shapeList = new ArrayList<>();
        shapeList.add(point1);
        shapeList.add(point2);
        shapeList.add(point3);
        shapeList.add(point4);
        shapeList.add(line1);
        shapeList.add(line2);
        shapeList.add(line3);
        shapeList.add(line4);
        shapeList.add(triangle);
        shapeList.add(rectangle);
        shapeList.add(circle);

        shapeList.forEach(shape -> {
            System.out.println(shape);
            System.out.println("Периметр: " + shape.getLength());
            System.out.println("Площадь: " + shape.getArea());
        });

        Color color = Color.RED;
        System.out.println(color.getName());
    }
}
