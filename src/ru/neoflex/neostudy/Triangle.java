package ru.neoflex.neostudy;

public class Triangle extends Shape{

    private final Line ab;
    private final Line ac;
    private final Line bc;

    public Triangle(Line ab, Line ac, Line bc) {
        super("Треугольник");
        this.ab = ab;
        this.ac = ac;
        this.bc = bc;
    }

    @Override public double getLength() {
        double abDist = ab.getLength();
        double acDist = ac.getLength();
        double bcDist = bc.getLength();

        return abDist + acDist + bcDist;
    }

    @Override public double getArea() {
        double abDist = ab.getLength();
        double acDist = ac.getLength();
        double bcDist = bc.getLength();

        double p = (abDist + acDist + bcDist) / 2;
        return Math.sqrt(p * (p-abDist) * (p-bcDist) * (p-acDist));
    }

    @Override public String toString() {
        return "Triangle{" +
                "type=" + getType() +
                ", ab=" + ab +
                ", ac=" + ac +
                ", bc=" + bc +
                '}';
    }
}
