package ru.neoflex.neostudy;

public class Point extends Shape{

    private final double x;
    private final double y;

    public Point(double x, double y) {
        super("Точка");
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    @Override public double getLength() {
        return 0;
    }

    @Override public double getArea() {
        return 0;
    }

    @Override public String toString() {
        return "Point{" +
                "type=" + getType() +
                ", x=" + getX() +
                ", y=" + getY() +
                '}';
    }
}
