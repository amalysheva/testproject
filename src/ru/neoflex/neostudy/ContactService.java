package ru.neoflex.neostudy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class ContactService {

    private static final List<Contact> contacts = new ArrayList<>();

    public static void addContact(Contact newContact) {
        Optional<Contact> foundContact  = getContact(newContact.getName());
        if (foundContact.isEmpty()) {
            contacts.add(newContact);
        }
    }

    public static Optional<Contact> getContact(String name) {
        return contacts.stream().filter(c -> c.getName().equals(name)).findFirst();
    }

    public static List<String> getPhonesByName(String name) {
        return getContact(name).map(Contact::getPhones).orElse(Collections.emptyList());
    }

    public static List<Contact> getContactLikeName(String name) {
        return contacts.stream().filter(c -> c.getName().contains(name)).collect(Collectors.toList());
    }
}
